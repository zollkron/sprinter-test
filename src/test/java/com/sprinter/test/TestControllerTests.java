package com.sprinter.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sprinter.test.model.Articulo;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TestControllerTests {

	@Autowired
	private MockMvc mockMvc; // Utilizamos MockMVC para las pruebas unitarias de los Servicios REST

	private static final Logger logger = LoggerFactory.getLogger(TestControllerTests.class);

	//En los tests lanzamos las excepciones en lugar de capturarlas para que no construya
	//el jar si ha fallado alguno de los test. Esto nos permitirá que sólo se construya
	//el jar si la aplicación sigue siendo robusta, ya que está orientada a TDD. Esto
	//es bueno sobre todo cuando se haga Refactoring del código o se extienda la aplicación.
	@Test
	public void testListarArticulos() throws Exception { 

		logger.info("Ejecutando testListarArticulos()");
		this.mockMvc.perform(get("/sprinter-test/listar-articulos")).andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$[0].nombre").exists()); // Primer elemento del array JSON
	}

	// Método privado auxiliar para realizar una de las pruebas.
	// Como mejora futura se podría añadir a una clase JsonTools dentro de un nuevo 
	// paquete llamado, por ejemplo, com.sprinter.test.utils.
	private static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void testInsertarArticulo() throws Exception {

		logger.info("Ejecutando testInsertarArticulo()");
		this.mockMvc
				.perform(MockMvcRequestBuilders.post("/sprinter-test/insertar-articulo")
						.content(asJsonString(
								new Articulo("Nombre", "Descripcion", "Caracteristicas", 19.34f, new Date())))
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated()).andExpect(MockMvcResultMatchers.jsonPath("$.nombre").exists());
	}

	@Test
	public void testObtenerArticulo() throws Exception {

		logger.info("Ejecutando testObtenerArticulo()");
		this.mockMvc.perform(get("/sprinter-test/obtener-articulo").param("idArticulo", "1")).andDo(print())
				.andExpect(status().isOk()).andExpect(jsonPath("$.nombre").exists()); // Sólo hay un elemento JSON
	}

	@Test
	public void testBorrarArticulo() throws Exception {

		logger.info("Ejecutando testBorrarArticulo()");
		this.mockMvc.perform(get("/sprinter-test/borrar-articulo").param("idArticulo", "8")).andDo(print())
				.andExpect(status().isOk()); // No devuelve JSON
		// Por lo que nos aseguramos de que nos devuelva un status OK como respuesta
	}

	@Test
	public void testListarPedidos() throws Exception {

		logger.info("Ejecutando testListarPedidos()");
		this.mockMvc.perform(get("/sprinter-test/listar-pedidos")).andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$[0].total").exists());
	}

	@Test
	public void testListarLineaPedidos() throws Exception {

		logger.info("Ejecutando testListarLineaPedidos()");
		this.mockMvc.perform(get("/sprinter-test/listar-lineas-pedido")).andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$[0].cantidad").exists());
	}

	@Test
	public void testListarLineasPedidoPorPedido() throws Exception {

		logger.info("Ejecutando testListarLineasPedidosPorPedido()");
		this.mockMvc.perform(get("/sprinter-test/listar-lineas-pedido-por-pedido").param("idPedido", "1"))
				.andDo(print()).andExpect(status().isOk()).andExpect(jsonPath("$[0].cantidad").exists());
	}
	
	//Si algo no da tiempo a implementarlo, o se piensa hacer en un futuro, se marcan los 
	//correspondientes '_TODO' para que nos sirvan de recordatorio.

	// TODO Hacer el resto de pruebas para el resto de la funcionalidad de los
	// repositorios CRUD, aunque no tengan un método REST asociado.
	// TODO Hacer pruebas para el controlador principal de la aplicación si se añade
	// funcionalidad para el mismo.
	// TODO Dividir esta clase en subclases correspondientes a las pruebas de los
	// servicios si se hace demasiado grande.
}
