-- Parte DDL de creación de tablas. Prefiero hacerla de forma manual aunque soy consciente
-- de que Spring JPA las define automáticamente. Pero de este modo puedo aprovechar el script
-- de inicialización SQL de H2 para redefinir y optimizar en memoria el tipo de los campos 
-- e insertarle datos posteriormente también.

DROP TABLE IF EXISTS linea_pedido CASCADE;
DROP TABLE IF EXISTS pedido CASCADE;
DROP TABLE IF EXISTS articulo CASCADE;

CREATE TABLE articulo (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(255) NOT NULL,
  descripcion VARCHAR(1023) NOT NULL,
  caracteristicas ARRAY,
  precio FLOAT NOT NULL,
  ultima_modificacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
);

CREATE TABLE pedido (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  total FLOAT NOT NULL,
  ultima_modificacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
);

CREATE TABLE linea_pedido (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  id_articulo INT NOT NULL,
  id_pedido INT NOT NULL,
  cantidad INT NOT NULL,
  ultima_modificacion TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  FOREIGN KEY (id_articulo) REFERENCES articulo(id),
  FOREIGN KEY (id_pedido) REFERENCES pedido(id)
);

-- Aprovecho la parte DML para insertar algunos datos de modo que las tablas no estén vacías.

INSERT INTO articulo VALUES 
(1,'adidas RunFalcon','Zapatillas de running para hombre adidas Falcon. Cómodas y ligeras, ofrecen un gran amortiguación y comodidad. Cuentan con un upper despejado, confeccionado en malla ligera transpirable. La mediasuela es de Eva y la suela de goma resistente a la abrasión.', 
	('Peso: 271 gr.','Drop: 10 mm.','Tejido exterior de malla transpirable.','Forro textil.','Mediasuela: Eva.'), 39.99, CURRENT_TIMESTAMP),
(2,'Calcetines Invisibles Fila','Pack de 6 pares de calcetines Fila para hombre. Diseño de corte invisible en diferentes colores.', 
	('El pack cuenta con 6 pares.','Costuras: diseñado con las menores costuras para evitar irritaciones.',
		'Tejido de punto transpirable.','Altura: corte por encima del tobillo','Talla única: 40-46'), 2.99, CURRENT_TIMESTAMP),
(3,'Bandolera Fila','Bandolera Fila en color negro con serigrafía de la marca a contraste y bolsillo adosado', 
	('Asa ajustables.','Tejido ligero y resistente.','Cuenta con bolsillo exterior.','Cierre de cremallera.',
		'Medidas: 27 x 37,5 x 10,5 cm (alto x largo x ancho)'), 19.99, CURRENT_TIMESTAMP),
(4,'Mochila Nike Elemental','Descubre la mochila Nike Elemental perfecta para el colegio o instituto. Diseñada en color gris con logotipo superior en rosa. Cuenta con correas ajustables y parte trasera con foam. Incorpora estuche.', 
	('Bolsillo exterior abiertos.','Asa superior para fácil transporte.','Incorpora estuche.','Correas ajustables.',
		'Parte trasera acolchada con foam.','Medidas: (alto x ancho x profundidad).'), 14.99, CURRENT_TIMESTAMP),
(5,'adidas Linear Classic','Mochila adidas Linear Classic. Perfecta para transportar tus cosas al gym. Diseñada en color negro con bolsillo exterior y cremallera a contraste en blanco. Logotipo frontal. Las asas ajustables y acolchadas te permiten portarla con facilidad.', 
	('Asas acolchadas y ajustables.','Tejido ligero y resistente.','Cuenta con bolsillo exterior.','Cierre de cremallera.',
		'Parte trasera con foam para mayor confort.'), 14.99, CURRENT_TIMESTAMP),
(6,'Mochila Fila','Descubre la mochila Fila Neal diseñada en color azul marino con serigrafía frontal a contraste. Cuenta con dos compartimentos principales con cierre de cremallera. Las asas acolchadas y ajustables y la parte trasera con foam favorecen el confort.', 
	('Asas acolchadas y ajustables.','Tejido ligero y resistente.','Cuenta con dos compartimentos principales.','Cierre de cremallera.',
		'Parte trasera con foam para mayor confort.'), 11.99, CURRENT_TIMESTAMP),
(7,'Fila Sling Sack','Moderna riñonera Fila con diseño en color negro y bolsillos de cremallera. Cierre ajustable.', 
	('Tejido ligero y resistente.','Cuenta con un bolsillo adosado.','Cierre de cremallera.','Medidas: 13x35x5 cm'), 14.99, CURRENT_TIMESTAMP);

INSERT INTO pedido VALUES 
(1, 42.98, CURRENT_TIMESTAMP);


INSERT INTO linea_pedido VALUES
(1,1,1,1,CURRENT_TIMESTAMP),
(2,2,1,1,CURRENT_TIMESTAMP);

