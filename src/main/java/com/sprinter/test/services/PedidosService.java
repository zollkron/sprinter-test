package com.sprinter.test.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sprinter.test.controller.JavaTestController;
import com.sprinter.test.dao.PedidosRepository;
import com.sprinter.test.model.Pedido;

@RestController
@RequestMapping(path = "/sprinter-test")
public class PedidosService {
	
	private static final Logger logger = LoggerFactory.getLogger(JavaTestController.class);
	
	@Autowired
	private PedidosRepository pedidosRepo;
	
	@GetMapping("/listar-pedidos")
	public Iterable<Pedido> getPedidos() {
		Iterable<Pedido> pedidos = null;
		try {
			pedidos = pedidosRepo.findAll();
			for (Pedido pedido : pedidos) {
				logger.info(pedido.toString());
				pedido.getLineasPedido(); // Ahora es cuando hacemos el fetch porque estaba en Lazy
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return pedidos;
	}
	
	//Si algo no da tiempo a implementarlo, o se piensa hacer en un futuro, se marcan los 
	//correspondientes '_TODO' para que nos sirvan de recordatorio.
	
	// TODO: Implementar el resto de métodos para Pedidos como se han implementado para Artículos.

}
