package com.sprinter.test.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sprinter.test.controller.JavaTestController;
import com.sprinter.test.dao.LineasPedidoRepository;
import com.sprinter.test.dao.PedidosRepository;
import com.sprinter.test.model.LineaPedido;

@RestController
@RequestMapping(path = "/sprinter-test")
public class LineasPedidoServices {
	
private static final Logger logger = LoggerFactory.getLogger(JavaTestController.class);

	@Autowired
	private PedidosRepository pedidosRepo;

	@Autowired
	private LineasPedidoRepository lineasPedidoRepo;

	@GetMapping("/listar-lineas-pedido")
	public Iterable<LineaPedido> getLineasPedido() {
		Iterable<LineaPedido> lineasPedidos = null;
		try {
			lineasPedidos = lineasPedidoRepo.findAll();
			logger.info("Líneas de Pedido encontradas en findAll():");
			//Aprovechamos Java 8 para hacer alguna lambda expresión.
			//En este caso, para cada una de las líneas de pedido 
			//mostramos su método toString() en el log.
	        lineasPedidos.forEach(lineaPedido -> logger.info(lineaPedido.toString())); 
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return lineasPedidos;
	}

	@GetMapping("/listar-lineas-pedido-por-pedido")
	public Iterable<LineaPedido> getLineasPedidoPorId(@RequestParam(value = "idPedido") int idPedido) {
		Iterable<LineaPedido> lineasPedidos = null;
		try {
			lineasPedidos = lineasPedidoRepo.findAllByPedidoJPA(pedidosRepo.findById(idPedido));
			logger.info("Líneas de Pedido encontradas en findByAllByPedidoJPA(" + idPedido + "):");
			//Aprovechamos Java 8 para hacer alguna lambda expresión.
			//En este caso, para cada una de las líneas de pedido 
			//mostramos su método toString() en el log.
	        lineasPedidos.forEach(lineaPedido -> logger.info(lineaPedido.toString())); 
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return lineasPedidos;
	}
	
	//Si algo no da tiempo a implementarlo, o se piensa hacer en un futuro, se marcan los 
	//correspondientes '_TODO' para que nos sirvan de recordatorio.
	
	// TODO: Implementar el resto de métodos para Líneas de Pedido como se han implementado para Artículos.


}
