package com.sprinter.test.services;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sprinter.test.controller.JavaTestController;
import com.sprinter.test.dao.ArticulosRepository;
import com.sprinter.test.model.Articulo;

@RestController
@RequestMapping(path = "/sprinter-test")
public class ArticulosService {
	
	private static final Logger logger = LoggerFactory.getLogger(JavaTestController.class);
	
	@Autowired
	private ArticulosRepository articulosRepo;

	@GetMapping("/listar-articulos")
	public Iterable<Articulo> getArticulos() {
		Iterable<Articulo> articulos = null;
		try {
			articulos = articulosRepo.findAll();
			logger.info("Artículos encontrados en findAll():");
			//Aprovechamos Java 8 para hacer alguna lambda expresión.
			//En este caso, para cada uno de los artículos mostramos 
			//su método toString() en el log.
	        articulos.forEach(articulo -> logger.info(articulo.toString())); 
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return articulos;
	}
	
	@GetMapping("/obtener-articulo")
	public Optional<Articulo> getArticulo(@RequestParam(value = "idArticulo") int idArticulo) {
		Optional<Articulo> articulo = null;
		try {
			articulo = articulosRepo.findById(idArticulo);
			logger.info("Artículo con id " + idArticulo + " encontrado: " + articulo.toString());
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return articulo;
	}

	@PostMapping(path = "/insertar-articulo", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Articulo> addArticulo(@Valid @RequestBody Articulo articulo) throws Exception {
		ResponseEntity<Articulo> respuesta = null;
		try {
			logger.info("Añadiendo artículo: " + articulo.toString());
			//Añade el artículo y devuelve un objeto respuesta con la entidad resultante
			respuesta = new ResponseEntity<Articulo>(articulosRepo.save(articulo), HttpStatus.CREATED);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		return respuesta;
	}
	
	@GetMapping("/borrar-articulo")
	public void deleteArticulo(@RequestParam(value = "idArticulo") int idArticulo) {
		try {
			Articulo articulo = articulosRepo.findById(idArticulo).get();
			logger.info("Borrando el artículo " + articulo.toString());
			articulosRepo.delete(articulo);
			logger.info("Artículo borrado correctamente");
		} catch (Exception e) {
			logger.info("El artículo no se pudo borrar por la siguiente excepción" 
					+ e.getMessage());
		}
	}

}
