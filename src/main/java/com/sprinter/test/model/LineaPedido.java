package com.sprinter.test.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties(value = {"pedidoJPA"})
public class LineaPedido {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_articulo", referencedColumnName = "id")
    private Articulo articulo;
	
	@OneToOne (fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_pedido", referencedColumnName = "id")
    private Pedido pedidoJPA;
	
	//Ésta será la propiedad "pedido" que serializaremos con el JSON para que no nos
	//haga el fetch de Pedido en la propiedad JPA y así evitar el bucle infinito JPA.
	//En su lugar haremos un "clone" con una instancia nueva distinta a la de JPA.
	//Con la anotación Transient le decimos a JPA que no mapee este atributo.
	@Transient
	private PedidoJSON pedido;
	
	private int cantidad;
    private Date ultimaModificacion;
    
    
    public LineaPedido() {
        super();
    }

	public LineaPedido(int cantidad, Date ultimaModificacion) {
		this.cantidad = cantidad;
		this.ultimaModificacion = ultimaModificacion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Articulo getArticulo() {
		return articulo;
	}

	public void setArticulo(Articulo articulo) {
		this.articulo = articulo;
	}
	
	public Pedido getPedidoJPA() {
		return pedidoJPA;
	}

	public void setPedidoJPA(Pedido pedidoJPA) {
		this.pedidoJPA = pedidoJPA;
	}

	public float getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Date getUltimaModificacion() {
		return ultimaModificacion;
	}

	public void setUltimaModificacion(Date ultimaModificacion) {
		this.ultimaModificacion = ultimaModificacion;
	}

	//Aquí clonamos la entidad JPA en el POJO que antes nos hemos definido
	//para evitar el ciclo JPA. Como se puede apreciar en el POJO no se 
	//incluyen nuevamente las líneas de pedido, de este modo aquí paramos el bucle JPA.
	public PedidoJSON getPedido() {
		pedido = new PedidoJSON();
		pedido.setId(pedidoJPA.getId());
		pedido.setTotal(pedidoJPA.getTotal());
		pedido.setUltimaModificacion(pedidoJPA.getUltimaModificacion());
		return pedido;
	}
	
	@Override
	public String toString() {
		return "LineaPedido [id=" + id + ", articulo=" + articulo + ", pedido=" + pedido + ", cantidad=" + cantidad
				+ ", ultimaModificacion=" + ultimaModificacion + "]";
	}
        
}