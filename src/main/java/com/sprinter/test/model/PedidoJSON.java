package com.sprinter.test.model;

import java.util.Date;

//Nos definimos un nuevo POJO independiente de JPA para poder serializarlo en JSON
//sin que hayan ciclos JPA.
public class PedidoJSON {

    private int id;
	private float total;
    private Date ultimaModificacion;
    
    public PedidoJSON() {
        super();
    }

	public PedidoJSON(float total, Date ultimaModificacion) {
		this.total = total;
		this.ultimaModificacion = ultimaModificacion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}

	public Date getUltimaModificacion() {
		return ultimaModificacion;
	}

	public void setUltimaModificacion(Date ultimaModificacion) {
		this.ultimaModificacion = ultimaModificacion;
	}


	@Override
	public String toString() {
		return "Pedido [id=" + id + ", total=" + total + ", ultimaModificacion=" + ultimaModificacion
				+ "]";
	}
        
}