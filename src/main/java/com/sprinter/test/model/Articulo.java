package com.sprinter.test.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties(value = {"lineasPedido"})
public class Articulo {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
	private String nombre;
	private String descripcion;
	private String caracteristicas;
	private float precio;
    private Date ultimaModificacion;
    
    @OneToMany(mappedBy = "pedidoJPA", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<LineaPedido> lineasPedido;
    
    public Articulo() {
        super();
    }

	public Articulo(String nombre, String descripcion, String caracteristicas, float precio, Date ultimaModificacion) {
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.caracteristicas = caracteristicas;
		this.precio = precio;
		this.ultimaModificacion = ultimaModificacion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCaracteristicas() {
		return caracteristicas;
	}

	public void setCaracteristicas(String caracteristicas) {
		this.caracteristicas = caracteristicas;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public Date getUltimaModificacion() {
		return ultimaModificacion;
	}

	public void setUltimaModificacion(Date ultimaModificacion) {
		this.ultimaModificacion = ultimaModificacion;
	}

	public Set<LineaPedido> getLineasPedido() {
		return lineasPedido;
	}

	public void setLineasPedido(Set<LineaPedido> lineasPedido) {
		this.lineasPedido = lineasPedido;
	}

	@Override
	public String toString() {
		return "Articulo [id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + ", caracteristicas="
				+ caracteristicas + ", precio=" + precio + ", ultimaModificacion=" + ultimaModificacion
				+ "]";
	}
        
}