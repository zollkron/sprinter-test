package com.sprinter.test.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Pedido {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
	private float total;
    private Date ultimaModificacion;
    
    @OneToMany(mappedBy = "pedidoJPA", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<LineaPedido> lineasPedido;
    
    public Pedido() {
        super();
    }

	public Pedido(float total, Date ultimaModificacion) {
		this.total = total;
		this.ultimaModificacion = ultimaModificacion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}

	public Date getUltimaModificacion() {
		return ultimaModificacion;
	}

	public void setUltimaModificacion(Date ultimaModificacion) {
		this.ultimaModificacion = ultimaModificacion;
	}

	public Set<LineaPedido> getLineasPedido() {
		return lineasPedido;
	}

	public void setLineasPedido(Set<LineaPedido> lineasPedido) {
		this.lineasPedido = lineasPedido;
	}

	@Override
	public String toString() {
		return "Pedido [id=" + id + ", total=" + total + ", ultimaModificacion=" + ultimaModificacion
				+ "]";
	}
        
}