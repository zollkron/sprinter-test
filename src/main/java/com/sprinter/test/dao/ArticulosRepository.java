package com.sprinter.test.dao;

import java.util.Optional;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;

import com.sprinter.test.model.Articulo;

public interface ArticulosRepository extends CrudRepository<Articulo, Integer> {
	
	//Si no está cacheado ejecutamos el método y ponemos el resultado en caché 
	//y si lo está no ejecutamos el método
	@Cacheable("cacheArticulos")
    Optional<Articulo> findById(int id);

	//Si no está cacheado ejecutamos el método y ponemos el resultado en caché 
	//y si lo está no ejecutamos el método
    @Cacheable("cacheArticulos")
    //Limpiamos la caché si todas las entradas se borran
    @CacheEvict(value = "cacheArticulos", allEntries = true)
    Iterable<Articulo> findAll();

    @SuppressWarnings("unchecked")
	@Override
    @CachePut("cacheArticulos") //Actualizamos la caché con el resultado obtenido
    Articulo save(Articulo articulo);

    @Override
    @CacheEvict("cacheArticulos") //Desalojamos la entrada de esta entity de la caché
    void delete(Articulo articulo);
	
}
