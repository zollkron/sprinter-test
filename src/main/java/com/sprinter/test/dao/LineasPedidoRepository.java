package com.sprinter.test.dao;

import java.util.Optional;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;

import com.sprinter.test.model.LineaPedido;
import com.sprinter.test.model.Pedido;

public interface LineasPedidoRepository extends CrudRepository<LineaPedido, Integer> {
	
	//Si no está cacheado ejecutamos el método y ponemos el resultado en caché 
	//y si lo está no ejecutamos el método
	@Cacheable("cacheLineasPedido")
    Optional<LineaPedido> findById(int id);

	//Si no está cacheado ejecutamos el método y ponemos el resultado en caché 
	//y si lo está no ejecutamos el método
    @Cacheable("cacheLineasPedido")
    //Limpiamos la caché si todas las entradas se borran
    @CacheEvict(value = "cacheLineasPedidos", allEntries = true)
    Iterable<LineaPedido> findAll();

    @SuppressWarnings("unchecked")
	@Override
    @CachePut("cacheLineasPedido") //Actualizamos la caché con el resultado obtenido
    LineaPedido save(LineaPedido lineaPedido);

    @Override
    @CacheEvict("cacheLineasPedido") //Desalojamos la entrada de esta entity de la caché
    void delete(LineaPedido lineaPedido);
	
	//Método personalizado para obtener las líneas de pedido a partir de un pedido concreto.
    //Se pone by PedidoJPA ya que esa es la propiedad que está mapeada en la entidad JPA.
    //No es necesario implementar este método porque "mágicamente" Spring ya lo 
    //autoimplementa en el Bean que Spring crea automáticamente a partir de esta interfaz.
    @Cacheable("cacheLineasPedido")
    //Limpiamos la caché si todas las entradas se borran
    @CacheEvict(value = "cacheLineasPedido", allEntries = true)
	public Iterable<LineaPedido> findAllByPedidoJPA(Optional<Pedido> pedido);
	
}
