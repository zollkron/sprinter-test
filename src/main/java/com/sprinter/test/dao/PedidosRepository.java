package com.sprinter.test.dao;

import java.util.Optional;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;

import com.sprinter.test.model.Pedido;

public interface PedidosRepository extends CrudRepository<Pedido, Integer> {
	
	//Si no está cacheado ejecutamos el método y ponemos el resultado en caché 
	//y si lo está no ejecutamos el método
	@Cacheable("cachePedidos")
    Optional<Pedido> findById(int id);

	//Si no está cacheado ejecutamos el método y ponemos el resultado en caché 
	//y si lo está no ejecutamos el método
    @Cacheable("cachePedidos")
    //Limpiamos la caché si todas las entradas se borran
    @CacheEvict(value = "cachePedidos", allEntries = true)
    Iterable<Pedido> findAll();

    @SuppressWarnings("unchecked")
	@Override
    @CachePut("cachePedidos") //Actualizamos la caché con el resultado obtenido
    Pedido save(Pedido pedido);

    @Override
    @CacheEvict("cachePedidos") //Desalojamos la entrada de esta entity de la caché
    void delete(Pedido pedido);
	
}
