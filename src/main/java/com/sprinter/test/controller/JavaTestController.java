package com.sprinter.test.controller;

import org.springframework.stereotype.Controller;

@Controller
public class JavaTestController {
	
	//Controlador principal de la aplicación que actualmente no hace nada, ya que delega en 
	//los controladores REST que actúan como fachada de la API de los servicios REST
	//(ver cada @RestController en las clases del paquete com.sprinter.test.services).
	//No obstante, se deja definido por si se añade otro tipo de funcionalidad que lo necesite.
	
}
